/*
 * PBVS_PositionMidLevelController.h
 *
 *  Created on: Nov 20, 2012
 *      Author: jespestana
 */

#ifndef DRONEPBVSPOSITIONMIDLEVELCONTROLLER_H_
#define DRONEPBVSPOSITIONMIDLEVELCONTROLLER_H_

#include "control/Controller_MidLevel_controlModes.h"
#include <math.h>
#include "control/PID.h"
#include "control/LowPassFilter.h"
#include "cvg_utils_library.h"
#include <iostream>
#include "xmlfilereader.h"

class PBVS_PositionMidLevelController
{
private:
	// Controller command inputs
    double xrel_ci, yrel_ci, zrel_ci, yawrel_ci;			// ci ~ command inputs
	// Controller state estimate inputs (controller feedback)
    double xrel_s, yrel_s, zrel_s, yawrel_s;
	// Intermediate variables between controller blocks
    double eps_x, eps_y, eps_yaw, eps_z;	// eps ~ epsilon to denote the control error in the commanded variable
    double vxc_int, vyc_int;				// c ~ command value; int ~ internal variable
    // Next control layer commands, in this case these commands are to be sent directly to the drone using the pelican proxy
    double pitchco, rollco, dyawco, dzco;			// co ~ command outputs (to pelican proxy)

    // Configuration parameters
    double MULTIROTOR_PBVSCONTROLLER_MAX_TILT;
    double MULTIROTOR_FAERO_DCGAIN_SPEED2TILT;
    double vxy_max;

	// Internal PID controllers
    // Note that that internal speed controller is substitued by constant gains
    CVG_BlockDiagram::PID pid_x, pid_y, pid_z, pid_yaw;
    void saturation_2D(double x1, double x2, double *y1, double *y2, double max);

    volatile bool started;
    Controller_MidLevel_controlMode::controlMode control_mode;

public:
    PBVS_PositionMidLevelController(int idDrone, const std::string &stackPath_in);
    ~PBVS_PositionMidLevelController();

	void reset();

    void setFeedback( double xrel_s_t, double yrel_s_t, double yawrel_s_t, double zrel_s_t);
    void setReference( double xrel_ci_t, double yrel_ci_t, double yawrel_ci_t, double zrel_ci_t);
    void getOutput( double *pitchco_t, double *rollco_t, double *dyawco_t, double *dzco_t);

	void setControlMode(Controller_MidLevel_controlMode::controlMode mode);
	Controller_MidLevel_controlMode::controlMode getControlMode();

public:
    void getCurrentPositionReference(double *xrel_ci_t, double *yrel_ci_t, double *zrel_ci_t, double *yawrel_ci_t);
};

#endif /* DRONEPBVSPOSITIONMIDLEVELCONTROLLER_H_ */
